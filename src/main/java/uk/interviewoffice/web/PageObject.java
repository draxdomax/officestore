package uk.interviewoffice.web;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class PageObject {

    protected WebDriver driver;

    public PageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void checkDisplayed(WebElement webElement){
        if (!webElement.isDisplayed()) throw new AssertionError("Element not displayed: " + webElement.getText());
    }
}
