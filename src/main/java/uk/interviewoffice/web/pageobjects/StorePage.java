package uk.interviewoffice.web.pageobjects;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uk.interviewoffice.web.PageObject;
import uk.interviewoffice.web.WebDriverFramework;

import java.util.concurrent.TimeUnit;


public class StorePage extends PageObject {

    @FindBy(xpath="//*[@id=\"logout\"]")
    public WebElement logOutBtn;

    @FindBy(xpath="/html/body/table/tbody/tr[2]/td[1]/span/a/img")
    public WebElement pencil;


    public StorePage(WebDriver driver) {
        super(driver);
    }

    public LogInPage logOut () {
        this.logOutBtn.click();
        return new LogInPage(driver);
    }
}
