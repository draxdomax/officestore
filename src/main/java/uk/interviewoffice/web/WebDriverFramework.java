package uk.interviewoffice.web;

public abstract class WebDriverFramework {
    public static int latency = 5;
    public static final String CONF_KEY_CHROME = "webdriver.chrome.driver";
    public static final String CONF_VALUE_CHROME = "bin/chromedriver.exe";
}

