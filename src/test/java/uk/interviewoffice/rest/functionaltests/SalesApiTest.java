package uk.interviewoffice.rest.functionaltests;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;

public class SalesApiTest {

    @Test
    public void example () {
        Response response = RestAssured
                .given()
                    .baseUri("http://eyqek.mocklab.io/")
                    .basePath("sales/")
                .when()
                    .get("login")
                .then()
                    .assertThat().statusCode(200)
                    .contentType(ContentType.JSON).extract().response();

        String version = response.jsonPath().get("version").toString();
        System.out.println(version);
    }
}
